.. SPDX-License-Identifier: GPL-3.0-only
.. SPDX-FileCopyrightText: 2020 Vincent Lequertier <vi.le@autistici.org>


FairLoss
========

The goal of this loss function is to take fairness into account during the
training of a PyTorch model. It works by adding a fairness measure to a regular
loss. Both the loss function and scores are provided.

.. code:: python

   import torch
   from fair_loss import FairLoss

   model = torch.nn.Sequential(torch.nn.Linear(5, 1), torch.nn.ReLU())
   data = torch.randint(0, 5, (100, 5), dtype=torch.float, requires_grad=True)
   y_true = torch.randint(0, 5, (100, 1), dtype=torch.float)
   y_pred = model(data)
   # Let's say the sensitive attribute is in the second dimension
   dim = 1
   criterion = FairLoss(torch.nn.MSELoss(), data[:, dim].detach().unique(), 'accuracy')
   loss = criterion(data[:, dim], y_pred, y_true)
   loss.backward()

.. autoclass:: fair_loss.FairLoss
   :members:
.. toctree::
   :maxdepth: 2
   :caption: Contents:
